#include <iostream>
#include <string>

struct Bag
{
    std::string books[];
};


struct Student
{
    int Age = 0;
    int Height = 0;
    std::string Name = "";
    Bag myBag = nullptr;

    void GetInfo()
    {
        std::cout << "student struct" << std::endl;
    }
};


int main()
{
    Student a;
    Student* ptr = new Student();

    ptr->GetInfo();

    return 0;
}